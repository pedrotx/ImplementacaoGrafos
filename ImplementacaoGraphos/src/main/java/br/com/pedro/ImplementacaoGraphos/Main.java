package br.com.pedro.ImplementacaoGraphos;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.concurrent.TimeUnit;

public class Main {

	public static Grafo init(Grafo grafo, BufferedReader br, FileReader fr, int count)
			throws NumberFormatException, IOException {

		br = new BufferedReader(fr);

		long tempoPreencherGrafoMA = 0;
		long tempoPreencherGrafoLA = 0;
		long tempoInsereArestaMA = 0;
		long tempoInsereArestaLA = 0;
		long inicio = 0;

		String linha;

		while ((linha = br.readLine()) != null) {

			if (count == 0) {
				grafo.setN(Integer.parseInt(linha.split("\t")[0]));
				grafo.setM(Integer.parseInt(linha.split("\t")[1]));
				inicio = System.nanoTime();
				grafo.preencherMatrizAdjacencia(grafo.getN());
				tempoPreencherGrafoMA = System.nanoTime() - inicio;
				inicio = System.nanoTime();
				grafo.preencherListaAdjacencia(grafo.getN());
				tempoPreencherGrafoLA = System.nanoTime() - inicio;
				grafo.preencherVetoresBusca(grafo.getN(), grafo.getM());

			} else {
				grafo.setU(Integer.parseInt(linha.split("\t")[0]));
				grafo.setV(Integer.parseInt(linha.split("\t")[1]));
				for (int i = 1; i <= grafo.getN(); i++) {
					for (int j = 1; j <= grafo.getN(); j++) {
						if (grafo.getU() == i && grafo.getV() == j) {

							inicio = System.nanoTime();
							grafo.insereArestaMatrizAdjacencia(i, j);
							tempoInsereArestaMA = System.nanoTime() - inicio;
							grafo.insereArestaMatrizAdjacencia(j, i);
							inicio = System.nanoTime();
							grafo.insereArestaListaAdjacencia(i, j);
							tempoInsereArestaLA = System.nanoTime() - inicio;
							grafo.insereArestaListaAdjacencia(j, i);
						}

					}

				}

			}

			count++;
		}

		return grafo;
	}

	public static void main(String[] args) throws NumberFormatException, IOException {

		long tempoEhConexo = 0;
		long tempoTemCiclo = 0;
		long tempoEhFloresta = 0;
		long tempoEhArvore = 0;
		long tempoEhArvore2 = 0;

		long tempoInit = 0;

		long tempoBuscaCompleta = 0;
		long tempoBuscaLargura = 0;
		long tempoBuscaProfundidadeRecursaoMA = 0;
		long tempoBuscaProfundidade = 0;
		long tempoPreencherGrafoMA = 0;
		long tempoPreencherGrafoLA = 0;
		long tempoInsereArestaMA = 0;
		long tempoInsereArestaLA = 0;
		long inicio = 0;
		
		
		
	
		for (int i = 0; i < 10; i++) {
			Grafo grafo = new Grafo();
			BufferedReader br = null;
			FileReader fr = null;
			int count = 0;

			fr = new FileReader("grafo_basico_1.txt");
			br = new BufferedReader(fr);

			String linha;

			while ((linha = br.readLine()) != null) {

				if (count == 0) {
					grafo.setN(Integer.parseInt(linha.split("\t")[0]));
					grafo.setM(Integer.parseInt(linha.split("\t")[1]));
					inicio = System.nanoTime();
					grafo.preencherMatrizAdjacencia(grafo.getN());
					tempoPreencherGrafoMA += System.nanoTime() - inicio;
					inicio = System.nanoTime();
					grafo.preencherListaAdjacencia(grafo.getN());
					tempoPreencherGrafoLA += System.nanoTime() - inicio;
					grafo.preencherVetoresBusca(grafo.getN(), grafo.getM());


				} else {
					grafo.setU(Integer.parseInt(linha.split("\t")[0]));
					grafo.setV(Integer.parseInt(linha.split("\t")[1]));
				

					for (int w = 1; w <= grafo.getN(); w++) {
						for (int j = 1; j <= grafo.getN(); j++) {
							if (grafo.getU() == w && grafo.getV() == j) {

								inicio = System.nanoTime();
								grafo.insereArestaMatrizAdjacencia(w, j);
								tempoInsereArestaMA += System.nanoTime() - inicio;
								grafo.insereArestaMatrizAdjacencia(j, w);
								inicio = System.nanoTime();
								grafo.insereArestaListaAdjacencia(w, j);
								tempoInsereArestaLA+= System.nanoTime() - inicio;
								grafo.insereArestaListaAdjacencia(j, w);
							}

						}

					}

				}

				count++;
			}

			inicio = System.nanoTime();
			grafo = init(grafo, br, fr, count);
			tempoInit += System.nanoTime() - inicio;

			inicio = System.nanoTime();
			grafo.buscaCompleta(grafo.getAdj());
			tempoBuscaCompleta += System.nanoTime() - inicio;

			grafo = init(grafo, br, fr, count);

			inicio = System.nanoTime();
			grafo.buscaProfundidadeRecursaoMA(grafo.getMatrizAdjacencia(), 1, grafo.getVisitado(), grafo.getExplorada(),
					grafo.getDescoberta());
			tempoBuscaProfundidadeRecursaoMA += System.nanoTime() - inicio;

			grafo = init(grafo, br, fr, count);

			inicio = System.nanoTime();
			grafo.buscaLargura(1, grafo.getVisitado(), grafo.getExplorada(), grafo.getDescoberta());
			tempoBuscaLargura += System.nanoTime() - inicio;

			grafo = init(grafo, br, fr, count);

			inicio = System.nanoTime();
			grafo.buscaProfundidade(1, grafo.getExplorada(), grafo.getVisitado(), grafo.getDescoberta());
			tempoBuscaProfundidade += System.nanoTime() - inicio;

			inicio = System.nanoTime();
			grafo.ehConexo(grafo.getVisitado());
			tempoEhConexo += System.nanoTime() - inicio;

			inicio = System.nanoTime();
			grafo.temCiclo(grafo.getDescoberta());
			tempoTemCiclo += System.nanoTime() - inicio;

			inicio = System.nanoTime();
			grafo.ehFloresta(grafo.getDescoberta());
			tempoEhFloresta += System.nanoTime() - inicio;

			inicio = System.nanoTime();
			grafo.ehArvore(grafo.getDescoberta(), grafo.getVisitado());
			tempoEhArvore += System.nanoTime() - inicio;

			inicio = System.nanoTime();
			grafo.ehArvoreDois(grafo.getDescoberta(), grafo.getVisitado());
			tempoEhArvore2 += System.nanoTime() - inicio;

			
			
			/*
			 * Descomente a parte abaixo para visualizar os resultados
			 */
			// System.out.println("Vetor de vertices visitados");
			// for (int w = 1; w < grafo.getVisitado().length; w++) {
			// if (grafo.getVisitado()[w]) {
			// System.out.println("Vertice " + w + " visitado ");
			// }
			// }
			//
			//
			// for (int w = 1; w < grafo.getExplorada().length; w++) {
			// for (int j = 1; j < grafo.getExplorada()[w].length; j++) {
			// if (grafo.getExplorada()[w][j]) {
			// System.out.println("Aresta " + w + j + " explorada ");
			// }
			// }
			// }
			//
			// for (int w = 1; w < grafo.getDescoberta().length; w++) {
			// for (int j = 1; j < grafo.getDescoberta()[w].length; j++) {
			// if (grafo.getDescoberta()[w][j]) {
			// System.out.println("Aresta " + w + j + " descoberta ");
			// }
			// }
			// }

			// System.out.println("");
			// System.out.println("Vizinhos do vertice 1:" +
			// grafo.obtemVizinhosMatrizAdjacencia(1));
			// System.out.println("Primeira matriz de adjacencia");
			// System.out.println(grafo.getMatrizAdjacencia().length);
			// for (int i = 1; i < grafo.getMatrizAdjacencia().length; i++) {
			// for (int j = 1; j < grafo.getMatrizAdjacencia()[i].length; j++) {
			// System.out.print(grafo.getMatrizAdjacencia()[i][j] + " ");
			// }
			// System.out.println();
			//
			// }
			// System.out.println("Lista de adjacencia apos inserir um vertice");
			// grafo.insereVerticeListaAdjacencia();
			// for (int i = 1; i < grafo.getAdj().length; i++) {
			// System.out.print("Vizinhos de " + i + ": ");
			// System.out.println(grafo.getAdj()[i]);
			// }
			// grafo.insereVerticeMatrizAdjacencia();
			//
			// System.out.println("Matriz de adjacencia apos inserir um vertice");
			// for (int i = 1; i < grafo.getMatrizAdjacencia().length; i++) {
			// for (int j = 1; j < grafo.getMatrizAdjacencia()[i].length; j++) {
			// System.out.print(grafo.getMatrizAdjacencia()[i][j] + " ");
			// }
			// System.out.println();
			//
			// }
			// grafo.removeArestaMatrizAdjacencia(1, 2);
			// System.out.println("Segunda matriz de adjacencia com a aresta 1-2 removida");
			// for (int i = 1; i < grafo.getMatrizAdjacencia().length; i++) {
			// for (int j = 1; j < grafo.getMatrizAdjacencia()[i].length; j++) {
			// System.out.print(grafo.getMatrizAdjacencia()[i][j] + " ");
			// }
			// System.out.println();
			//
			// }
			// grafo.removeVerticeMatrizAdjacencia(1);
			//
			// System.out.println("Terceira Matriz com o vertice 1 removido");
			// for (int i = 1; i < grafo.getMatrizAdjacencia().length; i++) {
			// for (int j = 1; j < grafo.getMatrizAdjacencia()[i].length; j++) {
			// System.out.print(grafo.getMatrizAdjacencia()[i][j] + " ");
			// }
			// System.out.println();
			//
			// }
			// System.out.println("Vizinhos do vertice 1:" +
			// grafo.obtemVizinhosListaAdjacencia(1));
			// System.out.println("Primeira lista de adjacencia");
			// for (int i = 1; i < grafo.getAdj().length; i++) {
			// System.out.print("Vizinhos de " + i + ": ");
			// System.out.println(grafo.getAdj()[i]);
			// }
			//
			// grafo.removeArestaListaAdjacencia(1, 2);
			//
			// System.out.println("Segunda lista de adjacencia com a aresta 1-2 removida");
			// for (int i = 1; i < grafo.getAdj().length; i++) {
			// System.out.print("Vizinhos de " + i + ": ");
			// System.out.println(grafo.getAdj()[i]);
			// }
			//
			// grafo.removeVerticeListaAdjacencia(1);
			// System.out.println("Terceira lista de adjacencia com o vertice 1 removido ");
			// for (int i = 1; i < grafo.getAdj().length; i++) {
			// System.out.print("Vizinhos de " + i + ": ");
			// System.out.println(grafo.getAdj()[i]);
			// }

		}

		System.out.println("Tempo medio de execução InsereArestaLA: " + tempoInsereArestaLA / 10 + " nanossegundos");
		System.out.println("Tempo medio de execução InsereArestaMA: " + tempoInsereArestaMA / 10 + " nanossegundos");
		System.out.println("Tempo medio de execução PreencherGrafoLA: " + tempoPreencherGrafoLA / 10 + " nanossegundos");
		System.out.println("Tempo medio de execução PreencherGrafoMA: " + tempoPreencherGrafoMA / 10 + " nanossegundos");
		System.out.println("Tempo medio de execução buscaCompleta: " + tempoBuscaCompleta / 10 + " nanossegundos");
		System.out.println("Tempo medio de execução buscaProfundidadeRecursaoMA: "
				+ tempoBuscaProfundidadeRecursaoMA / 10 + " nanossegundos");
		System.out.println(
				"Tempo medio de execução buscaProfundidade: " + tempoBuscaProfundidade / 10 + " nanossegundos");
		System.out.println("Tempo medio de execução buscaLargura: " + tempoBuscaLargura / 10 + " nanossegundos");
		System.out.println("Tempo medio de execução ehConexo: " + (tempoEhConexo / 10) + " nanossegundos");
		System.out.println("Tempo medio de execução temCiclo: " + tempoTemCiclo / 10 + " nanossegundos");
		System.out.println("Tempo medio de execução ehFloresta: " + tempoEhFloresta / 10 + " nanossegundos");
		System.out.println("Tempo medio de execução ehArvore: " + tempoEhArvore / 10 + " nanossegundos");
		System.out.println("Tempo medio de execução ehArvore2: " + tempoEhArvore2 / 10 + " nanossegundos");

	}
}
