package br.com.pedro.ImplementacaoGraphos;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Stack;

public class Grafo {

	private int n;// numero de vertices
	private int m;// numero de arestas

	private int u;
	private int v;

	private boolean[][] matrizAdjacencia;
	private boolean[] visitado;
	private boolean[][] explorada;
	private boolean[][] descoberta;
	private List<List<Integer>> grafo;
	private List<Integer>[] adj;

	public void preencheGrafo(int u, int v) {
		for (List<Integer> linhaGrafo : grafo) {

		}
	}


	public void preencherMatrizAdjacencia(int numeroVertices) {
		matrizAdjacencia = new boolean[numeroVertices + 1][numeroVertices + 1];
	}

	public void preencherVetoresBusca(int numeroVertices, int numeroArestas) {
		visitado = new boolean[numeroVertices + 1];
		explorada = new boolean[numeroVertices + 1][numeroVertices + 1];
		descoberta = new boolean[numeroVertices + 1][numeroVertices + 1];
	}

	public void buscaCompleta(List<Integer>[] listaAdjacencia) {
		for (int i = 1; i < visitado.length; i++) {
			if (!visitado[i]) {
				busca(listaAdjacencia, i);
			}
		}
	}

	public void busca(List<Integer>[] listaAdjacencia, int u) {

		for (int i = 1; i < matrizAdjacencia.length; i++) {
			for (int j = i + 1; j < matrizAdjacencia[i].length; j++) {
				if (matrizAdjacencia[i][j]) {
					visitado[i] = true;
					if (visitado[i] && !explorada[i][j]) {
						explorada[i][j] = true;
						if (!visitado[j]) {
							visitado[j] = true;
							descoberta[i][j] = true;

						}

					}
				}
			}
		}

	}

	public void buscaProfundidade(int v, boolean[][] explorada, boolean[] visitado, boolean[][] descoberta) {
		Stack pilha = new Stack();
		visitado[v] = true;

		for (int i = 0; i < obtemVizinhosMatrizAdjacencia(v).size(); i++) {
			int w = obtemVizinhosMatrizAdjacencia(v).get(i);

			if (visitado[w]) {
				if (!explorada[v][w]) {
					explorada[v][w] = true;
				}
			} else {
				explorada[v][w] = true;
				descoberta[v][w] = true;
				visitado[w] = true;
			}

		}
	}

	public void buscaLargura(int v, boolean[] visitado, boolean[][] explorada, boolean[][] descoberta) {
		LinkedList f = new LinkedList();
		visitado[v] = true;
		f.addFirst(v);
		while (f.size() > 0) {
			f.removeFirst();
			for (int i = 0; i < obtemVizinhosMatrizAdjacencia(v).size(); i++) {
				int w = obtemVizinhosMatrizAdjacencia(v).get(i);
				if (visitado[w]) {
					if (!explorada[v][w]) {
						explorada[v][w] = true;
					}
				} else {
					explorada[v][w] = true;
					descoberta[v][w] = true;
					visitado[w] = true;
					f.addFirst(w);
				}
			}
		}
	}
	


	public int primeiroViz(int w) {

		List lista = obtemVizinhosMatrizAdjacencia(w);
		if (lista != null && !lista.isEmpty()) {
			return (Integer) lista.get(0);
		}
		return 0;
	}

	public int proximoViz(int v) {
		List lista = obtemVizinhosMatrizAdjacencia(v);
		if (lista != null && !lista.isEmpty()) {
			if (lista.size() > 1)
				return (Integer) lista.get(1);
		}
		return 0;
	}

	public void buscaProfundidadeRecursaoMA(boolean[][] matrizAdjacencia, int v, boolean[] visitado,
			boolean[][] explorada, boolean[][] descoberta) {

		visitado[v] = true;
		if (v + 1 < visitado.length) {
			int w = v + 1;
			if (matrizAdjacencia[v][w]) {
				if (visitado[w]) {
					if (!explorada[v][w]) {
						explorada[v][w] = true;
					}
				} else {
					explorada[v][w] = true;
					descoberta[v][w] = true;
					buscaProfundidadeRecursaoMA(matrizAdjacencia, w, visitado, explorada, descoberta);
				}

			}
		}

	}

	public boolean ehConexo(boolean[] visitado) {

		for (int i = 1; i < visitado.length; i++) {
			if (!visitado[i]) {
				return false;
			}
		}
		return true;

	}

	public boolean ehFloresta(boolean[][] descoberta) {
		return !temCiclo(descoberta);
	}

	public boolean ehArvore(boolean[][] descoberta, boolean[] visitado) {

		for (int i = 1; i < visitado.length; i++) {
			if (!visitado[i]) {
				return false;
			}
		}
		for (int i = 1; i < descoberta.length; i++) {
			for (int j = i + 1; j < descoberta[i].length; j++) {
				if (i != j) {
					if (matrizAdjacencia[i][j]) {
						if (!descoberta[i][j]) {
							return false;
						}
					}
				}
			}
		}
		return true;

	}

	public boolean ehArvoreDois(boolean[][] descoberta, boolean[] visitado) {
		return ehConexo(visitado) && !temCiclo(descoberta);
	}

	public boolean temCiclo(boolean[][] descoberta) {

		for (int i = 1; i < descoberta.length; i++) {
			for (int j = i + 1; j < descoberta[i].length; j++) {
				if (i != j) {
					if (matrizAdjacencia[i][j]) {
						if (!descoberta[i][j]) {
							return true;
						}
					}
				}
			}
		}
		return false;
	}

	public boolean[] getVisitado() {
		return visitado;
	}

	public void setVisitado(boolean[] visitado) {
		this.visitado = visitado;
	}

	public boolean[][] getExplorada() {
		return explorada;
	}

	public void setExplorada(boolean[][] explorada) {
		this.explorada = explorada;
	}

	public boolean[][] getDescoberta() {
		return descoberta;
	}

	public void setDescoberta(boolean[][] descoberta) {
		this.descoberta = descoberta;
	}

	public void preencherListaAdjacencia(int numeroVertices) {
		adj = (List<Integer>[]) new List[numeroVertices + 1];
		for (int i = 1; i <= numeroVertices; i++)
			adj[i] = new ArrayList<Integer>();
	
	}

	public void insereArestaMatrizAdjacencia(int u, int v) {		
		matrizAdjacencia[u][v] = true;
	}

	public void insereArestaListaAdjacencia(int u, int v) {
		adj[u].add(v);
	}

	public List<Integer> obtemVizinhosMatrizAdjacencia(int u) {
		List<Integer> arestas = new ArrayList<Integer>();
		for (int i = 1; i <= n; i++) {
			if (matrizAdjacencia[i][u] || matrizAdjacencia[u][i]) {
				arestas.add(i);
			}
		}
		return arestas;
	}

	public List<Integer> obtemVizinhosListaAdjacencia(int u) {

		return adj[u];
	}

	public void insereVerticeMatrizAdjacencia() {
		// long inicio = System.nanoTime();
		boolean[][] novaMatrizAdjacencia = new boolean[matrizAdjacencia.length + 1][matrizAdjacencia.length + 1];
		for (int i = 1; i < matrizAdjacencia.length; i++) {
			for (int j = 1; j < matrizAdjacencia.length; j++) {

				novaMatrizAdjacencia[i][j] = matrizAdjacencia[i][j];

			}

			novaMatrizAdjacencia[novaMatrizAdjacencia.length - 1][novaMatrizAdjacencia.length - 1] = false;

		}
		matrizAdjacencia = novaMatrizAdjacencia;
	}

	public void insereVerticeListaAdjacencia() {
		List<Integer>[] adj = (List<Integer>[]) new List[this.adj.length + 1];

		for (int i = 1; i < adj.length; i++) {
			if (i < this.adj.length) {
				adj[i] = this.adj[i];
			} else {
				adj[i] = new ArrayList<Integer>();
			}
		}

		this.adj = adj;
	}

	public void removeVerticeMatrizAdjacencia(int u) {
		for (int i = 1; i <= n; i++) {
			if (matrizAdjacencia[i][u] || matrizAdjacencia[u][i]) {
				removeArestaMatrizAdjacencia(u, i);
				removeArestaMatrizAdjacencia(i, u);
			}
		}
	}

	public void removeVerticeListaAdjacencia(int u) {
		adj[u].clear();
	}

	public void removeArestaMatrizAdjacencia(int u, int v) {
		matrizAdjacencia[u][v] = false;
	}

	public void removeArestaListaAdjacencia(int u, int v) {
		Iterator<Integer> it = adj[u].iterator();
		while (it.hasNext()) {
			if (it.next() == v) {
				it.remove();
				return;
			}
		}
	}

	public boolean existeArestaMatrizAdjacencia(int u, int v) {
		return matrizAdjacencia[u][v];
	}

	public boolean existeVerticeListaAdjacencia(int u, int v) {
		return adj[u].contains(v);
	}

	public int getU() {
		return u;
	}

	public void setU(int u) {
		this.u = u;
	}

	public int getV() {
		return v;
	}

	public void setV(int v) {
		this.v = v;
	}

	public int getN() {
		return n;
	}

	public void setN(int n) {
		this.n = n;
	}

	public int getM() {
		return m;
	}

	public void setM(int m) {
		this.m = m;
	}

	public boolean[][] getMatrizAdjacencia() {
		return matrizAdjacencia;
	}

	public void setMatrizAdjacencia(boolean[][] matrizAdjacencia) {
		this.matrizAdjacencia = matrizAdjacencia;
	}

	public List<Integer>[] getAdj() {
		return adj;
	}

	public void setAdj(List<Integer>[] adj) {
		this.adj = adj;
	}

}
