package br.com.pedro.ArvoreGeradoraMinima;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;


import br.com.pedro.ImplementacaoGraphos.Grafo;

public class MainPrim {

	public static void main(String[] args) throws NumberFormatException, IOException {

		int[][] grafoComPeso = null;
		BufferedReader br = null;
		FileReader fr = null;
		int count = 0;
		int numVertices = 0;
		int u = 0;
		int v = 0;
		fr = new FileReader("grafo_basico_1.txt");
		br = new BufferedReader(fr);

		String linha;

		while ((linha = br.readLine()) != null) {
			if (count == 0) {


				grafoComPeso = new int[numVertices + 1][numVertices + 1];
			} else {
				u = Integer.parseInt(linha.split("\t")[0]);
				v = Integer.parseInt(linha.split("\t")[1]);
				if (linha.split("\t").length > 2) {
					grafoComPeso[u][v] = Integer.parseInt(linha.split("\t")[2]);
					grafoComPeso[v][u] = Integer.parseInt(linha.split("\t")[2]);
				}
			}
			count++;
		}
		Prim arvoreMinima = new Prim(numVertices + 1);
		arvoreMinima.algoritmoDePrim(grafoComPeso);
		br.close();
	}
}
