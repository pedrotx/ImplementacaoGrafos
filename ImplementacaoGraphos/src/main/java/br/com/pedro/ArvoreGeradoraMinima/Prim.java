package br.com.pedro.ArvoreGeradoraMinima;

public class Prim {

	private int numVertices;

	public Prim(int numVertices) {
		this.numVertices = numVertices;
	}

	public int retornaPesoMinimo(int pesos[], Boolean verticesNaoIncluidos[]) {

		int pesoMinimo = Integer.MAX_VALUE, posPesoMinimo = -1;

		for (int v = 1; v < numVertices; v++)
			if (verticesNaoIncluidos[v] == false && pesos[v] < pesoMinimo) {
				pesoMinimo = pesos[v];
				posPesoMinimo = v;
			}

		return posPesoMinimo;
	}

	public void imprimeArvoreGeradoraMinima(int listaDePais[], int grafoComPeso[][]) {
		System.out.println("Aresta   Peso");
		for (int i = 2; i < numVertices; i++) {
			System.out.println(listaDePais[i] + " - " + i + "    " + grafoComPeso[i][listaDePais[i]]);
		}
	}

	public void algoritmoDePrim(int grafoComPeso[][]) {

		int listaDePais[] = new int[numVertices];

		int pesos[] = new int[numVertices];

		Boolean verticesNaoIncluidos[] = new Boolean[numVertices];

		for (int i = 1; i < numVertices; i++) {
			pesos[i] = Integer.MAX_VALUE;
			verticesNaoIncluidos[i] = false;
		}

		pesos[1] = 0;
		listaDePais[1] = -1;
		for (int i = 1; i < numVertices - 1; i++) {

			int u = retornaPesoMinimo(pesos, verticesNaoIncluidos);

			verticesNaoIncluidos[u] = true;

			for (int v = 1; v < numVertices; v++)
				if (grafoComPeso[u][v] != 0 && verticesNaoIncluidos[v] == false && grafoComPeso[u][v] < pesos[v]) {
					listaDePais[v] = u;
					pesos[v] = grafoComPeso[u][v];
				}
		}

	
		imprimeArvoreGeradoraMinima(listaDePais, grafoComPeso);
	}
}